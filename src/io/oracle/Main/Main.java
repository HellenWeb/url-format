
/**
* Main File
**/

package io.oracle.Main;

// Modules

import java.util.Scanner;
import java.lang.Exception;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.IOException;

// Main Class

public class Main {
  public static final Scanner scan = new Scanner(System.in);
  public static String url;
  public static final String RESET = "\u001B[0;1m";
  public static final String BLACK = "\u001B[30;1m";
  public static final String RED = "\u001B[31;1m";
  public static final String GREEN = "\u001B[32;1m";
  public static final String YELLOW = "\u001B[33;1m";
  public static final String BLUE = "\u001B[34;1m";
  public static final String PURPLE = "\u001B[35;1m";
  public static final String CYAN = "\u001B[36;1m";
  public static final String WHITE = "\u001B[37;1m";

  public static void main(String args[]) throws IOException {
    showLogo();
    showMenu();
    showResult();
  }

  private static void showLogo() {
    System.out.println(""
        + GREEN + "\n"
        + GREEN + " █╗░░░██╗██████╗░██╗░░░░░░░░░░░███████╗░█████╗░██████╗░███╗░░░███╗░█████╗░████████╗\n"
        + GREEN + "██║░░░██║██╔══██╗██║░░░░░░░░░░░██╔════╝██╔══██╗██╔══██╗████╗░████║██╔══██╗╚══██╔══╝\n"
        + GREEN + "██║░░░██║██████╔╝██║░░░░░█████╗█████╗░░██║░░██║██████╔╝██╔████╔██║███████║░░░██║░░░\n"
        + GREEN + "██║░░░██║██╔══██╗██║░░░░░╚════╝██╔══╝░░██║░░██║██╔══██╗██║╚██╔╝██║██╔══██║░░░██║░░░\n"
        + GREEN + "╚██████╔╝██║░░██║███████╗░░░░░░██║░░░░░╚█████╔╝██║░░██║██║░╚═╝░██║██║░░██║░░░██║░░░\n"
        + GREEN + "░╚═════╝░╚═╝░░╚═╝╚══════╝░░░░░░╚═╝░░░░░░╚════╝░╚═╝░░╚═╝╚═╝░░░░░╚═╝╚═╝░░╚═╝░░░╚═╝░░░\n"
        + GREEN + "                             Copyright by. Hellen\n\n");
  }

  private static void showMenu() {
    System.out.print("[*] Enter your url address: ");
    url = scan.next();
  }

  private static void showResult() {
    try {
      URL url_req = new URL(url);
      io.oracle.Request.Request req = new io.oracle.Request.Request(url_req);
      req.showPort();
      req.showProtocol();
      req.showRef();
      req.showDefaultPort();
      req.showFile();
      req.showAuth();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }
}
