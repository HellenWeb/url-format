
/**
* Request File 
**/

package io.oracle.Request;

// Modules

import java.net.URL;

// Request Class

public class Request {
  public URL req;
  public static final String RESET = "\u001B[0;1m";
  public static final String BLACK = "\u001B[30;1m";
  public static final String RED = "\u001B[31;1m";
  public static final String GREEN = "\u001B[32;1m";
  public static final String YELLOW = "\u001B[33;1m";
  public static final String BLUE = "\u001B[34;1m";
  public static final String PURPLE = "\u001B[35;1m";
  public static final String CYAN = "\u001B[36;1m";
  public static final String WHITE = "\u001B[37;1m";

  public Request(URL req) {
    this.req = req;
  }

  public void showProtocol() {
    System.out.println(BLUE + "[*] Protocol your url: " + this.req.getProtocol());
  }

  public void showPort() {
    System.out.println(BLUE + "\n[*] Port your url: " + this.req.getPort());
  }

  public void showRef() {
    System.out.println(BLUE + "[*] Reference or anchor your url: " + this.req.getRef());
  }

  public void showDefaultPort() {
    System.out.println(BLUE + "[*] Default Port your url: " + this.req.getDefaultPort());
  }

  public void showFile() {
    System.out.println(BLUE + "[*] File your url: " + this.req.getFile());
  }

  public void showAuth() {
    System.out.println(BLUE + "[*] Auth your url: " + this.req.getAuthority() + WHITE + "\n");
  }
}
