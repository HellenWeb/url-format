#!/bin/bash

# in directory project

echo -n -e "Install packages on (1. APT; 2. Pacman): "; read n

if [[ $n == "1" ]]; then
  sudo apt update
  sudo apt install default-jdk
  sudo apt install default-jre
elif [[ $n == "2" ]]; then
  sudo pacman -Sy
  sudo pacman -S jdk-openjdk
fi

# Creating class
sudo javac -d . src/io/oracle/Request/Request.java
sudo javac -d . src/io/oracle/Main/Main.java

# Creating jar file
echo -n -e "Compile jar file (y/N): "; read c

if [[ $c == "y" ]]; then
  sudo jar cvf urlformatapp.jar io/oracle/Main/Main.class io/oracle/Request/Request.class
fi
