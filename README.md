# Url Format | Java

This is a simple java script to display url data.

# Launching

> First, you need run ./setup.sh file

## First Way
```
java -classpath <path to app> io.oracle.Main.Main
```
## Second Way
```
java -jar urlformatapp.jar
```
# Copyright 
Copyright by. Hellen
